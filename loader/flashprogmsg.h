/*************************************************************************
 * FlashProg - programmer utility for low voltage SPI flash 
 *             memory devices.
 *
 * Copyright (C) 2015 Dilshan R Jayakody (4S6DRJ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/

#ifndef FLASHPROG_MSG_H_
#define FLASHPROG_MSG_H_

// Error messages related to file I/O.
#define ERR_OUTPUT_FILE_NOT_DEFINE "FlashProg: Output filename is not specified.\n"
#define ERR_FAIL_OUTPUT_FILE_CREATE "FlashProg: Unable to create output file.\n"
#define ERR_FILE_CLOSE_FAIL "FlashProg: Unable to close file.\n"
#define ERR_BIN_FILENAME_NULL "FlashProg: Binary filename is not specified.\n"
#define ERR_INPUT_FILE_CREATE_FAIL "FlashProg: Unable to open input file.\n"

// Error messages related to USB communication.
#define ERR_USB_OPEN "FlashProg: Unable to open USB flash programmer.\n"
#define ERR_USB_INVALID_RESPONSE "FlashProg: Invalid response or programmer is not functioning properly.\n"
#define ERR_USB_STATUS_FAIL "FlashProg: Unable to retrieve flash memory status data.\n"
#define ERR_USB_FLASH_ID_FAIL "FlashProg: Unable to retrieve flash memory identification bytes.\n"

// Error messages related to memory management.
#define ERR_MEM_FAIL "FlashProg: Internal memory error or insufficient memory.\n"

// General error messages.
#define ERR_PARAM "FlashProg: Invalid argument(s).\n"

#endif /* FLASHPROG_MSG_H_ */
