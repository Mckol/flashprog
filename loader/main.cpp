/*************************************************************************
 * FlashProg - programmer utility for low voltage SPI flash 
 *             memory devices.
 *
 * Copyright (C) 2015 Dilshan R Jayakody (4S6DRJ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/

#include "flashprog.h"
#include "flashprogmsg.h"

int main(int argc, char* argv[])
{
	if(argc == 1)
	{
		// If command line arguments are missing, show default help screen and close the application.
		ShowApplicationHelp();
		return EXIT_SUCCESS;
	}

	if((strcmp(argv[1], "-i") == 0) || (strcmp(argv[1], "--information") == 0))
	{
		// Display device ID and close the application.
		GetFlashMemoryDeviceID();
		return EXIT_SUCCESS;
	}
	else if((strcmp(argv[1], "-r") == 0) || (strcmp(argv[1], "--read") == 0))
	{
		if(argc >= 5)
		{
			// Read specified address range from memory device and write to file.
			unsigned int addressStart = 0, addressLength = 0, offset = 0;
			unsigned char readCount = 0, readBufferSize = 0;
			unsigned char *tempDataBuffer = NULL, animPos = 1, progressLevel = 1;
			char *fileName = NULL;
			FILE *binFile = NULL;

			// Obtain required commandline parameters.
			addressStart = atoi(argv[2]);
			addressLength = atoi(argv[3]);
			fileName = argv[4];

			if(fileName == NULL)
			{
				fprintf(stderr, ERR_OUTPUT_FILE_NOT_DEFINE);
				return EXIT_FAILURE;
			}

			// Create binary output file to write the capturing data.
			binFile = fopen(fileName, "wb");
			if(binFile == 0)
			{
				fprintf(stderr, ERR_FAIL_OUTPUT_FILE_CREATE);
				return EXIT_FAILURE;
			}

			printf("\nReading memory data: %c", animSeq[0]);

			while(addressLength > 0)
			{
				// Send read request to USB programmer device.
				readBufferSize = (addressLength > USBPROG_BUFFER_SIZE) ? USBPROG_BUFFER_SIZE : addressLength;
				readCount = ReadFlashMemoryBlock(addressStart + offset, readBufferSize, &tempDataBuffer);
				offset += readCount;
				addressLength -= readCount;

				// Write captured data to the output file stream.
				if((readCount > 0) && (tempDataBuffer != NULL))
				{
					fwrite(tempDataBuffer, 1, readCount, binFile);
					free(tempDataBuffer);
				}
				else
				{
					fprintf(stderr, ERR_MEM_FAIL);
					break;
				}

				// Update progress animation.
				if((++progressLevel) >= 12)
				{
					progressLevel = 0;
					printf("%c%c", 8, animSeq[animPos]);
					if((++animPos) > 3)
					{
						animPos = 0;
					}
				}
			}

			// Memory read operation is completed.
			printf("%c",8);
			printf("completed.\n");

			// Close output file.
			if(fclose(binFile) != 0)
			{
				fprintf(stderr, ERR_FILE_CLOSE_FAIL);
				return EXIT_FAILURE;
			}

			return EXIT_SUCCESS;
		}
		else
		{
		  // Number of arguments are mismatching.
		  fprintf(stderr, ERR_PARAM);
		  return EXIT_FAILURE;
		}
	}
	else if((strcmp(argv[1], "-e") == 0) || (strcmp(argv[1], "--erase") == 0))
	{
		unsigned char memStatus = 0, animPos = 1;

		// Initiate flash memory erasing process.
		printf("\nErasing flash memory contents: %c", animSeq[0]);
		memStatus = EraseFlashMemory();

		// Wait until memory erasing is complete.
		while((memStatus & 0x01) == 0x01)
		{
			sleep(200);
			memStatus = GetFlashMemoryStatus();

			// Update screen animation.
			printf("%c%c", 8, animSeq[animPos]);
			if((++animPos) > 3)
			{
				animPos = 0;
			}
		}

		// Erase operation completed.
		printf("%c",8);
		printf("completed.\n");

		return EXIT_SUCCESS;
	}
	else if((strcmp(argv[1], "-w") == 0) || (strcmp(argv[1], "--write") == 0))
	{
		if(argc >= 3)
		{
			// Write binary file to flash memory from specified starting address.
			unsigned char fileBuffer[USBPROG_WRITE_BUFFER_SIZE], memStatus = 0, animPos = 1;
			char *fileName = NULL;
			FILE *binFile = NULL;

			fileName = argv[2];

			if(fileName == NULL)
			{
				fprintf(stderr, ERR_BIN_FILENAME_NULL);
				return EXIT_FAILURE;
			}

			// Open specified binary file to read data.
			binFile = fopen(fileName, "rb");
			if(binFile == 0)
			{
				fprintf(stderr, ERR_INPUT_FILE_CREATE_FAIL);
				return EXIT_FAILURE;
			}

			// Send starting address to the flash programmer device.
			SetFlashWriteAddress(0);

			printf("\nProgramming flash memory contents: %c", animSeq[0]);

			while(!feof(binFile))
			{
				// Clear file data buffer.
				memset(fileBuffer, 0xFF, USBPROG_WRITE_BUFFER_SIZE);

				// Read next data block from input file.
				if(fread(fileBuffer, 1, USBPROG_WRITE_BUFFER_SIZE, binFile) > 0)
				{
					// Send data buffer to the flash programmer device.
					memStatus = WriteFlashMemoryPage(fileBuffer, USBPROG_WRITE_BUFFER_SIZE);

					// Wait until write operation is complete.
					while((memStatus & 0x01) == 0x01)
					{
						sleep(50);
						memStatus = GetFlashMemoryStatus();
					}
				}

				// Update screen animation.
				printf("%c%c", 8, animSeq[animPos]);
				if((++animPos) > 3)
				{
					animPos = 0;
				}
			}

			// Programming operation completed.
			printf("%c",8);
			printf("completed.\n");

			// Close input file.
			if(fclose(binFile) != 0)
			{
				fprintf(stderr, ERR_FILE_CLOSE_FAIL);
				return EXIT_FAILURE;
			}

			return EXIT_SUCCESS;
		}
    else
    {
      // Number of arguments are mismatching.
      fprintf(stderr, ERR_PARAM);
      return EXIT_FAILURE;
    }
	}
	else if((strcmp(argv[1], "-b") == 0) || (strcmp(argv[1], "-s") == 0) || (strcmp(argv[1], "--block-erase") == 0) || (strcmp(argv[1], "--sector-erase") == 0))
	{
		if(argc >= 3)
		{
			// Erase memory block related to given memory address.
			unsigned int blockAddress = 0;
			unsigned char memStatus = 0, animPos = 1, eraseCmd = DEVREQ_ERASE_BLOCK;

			if((strcmp(argv[1], "-s") == 0) || (strcmp(argv[1], "--sector-erase") == 0))
			{
				eraseCmd = DEVREQ_ERASE_SECTOR;
			}

			// Obtain required commandline parameters.
			blockAddress = atoi(argv[2]);

			// Send block address to the flash programmer device.
			SetFlashWriteAddress(blockAddress);
			memStatus = EraseFlashMemorySegment(eraseCmd);

			printf("\nErasing flash memory contents: %c", animSeq[0]);

			// Wait until memory erasing is complete.
			while((memStatus & 0x01) == 0x01)
			{
				sleep(200);
				memStatus = GetFlashMemoryStatus();

				// Update screen animation.
				printf("%c%c", 8, animSeq[animPos]);
				if((++animPos) > 3)
				{
					animPos = 0;
				}
			}

			// Erase operation completed.
			printf("%c",8);
			printf("completed.\n");

			return EXIT_SUCCESS;
		}
    else
    {
      // Number of arguments are mismatching.
      fprintf(stderr, ERR_PARAM);
      return EXIT_FAILURE;
    }
	}
	else if((strcmp(argv[1], "-g") == 0) || (strcmp(argv[1], "--set-status") == 0))
	{
		if(argc >= 3)
		{
			// Setup device status register value.
			unsigned char regVal = 0, memStatus = 0, animPos = 1;

			// Obtain required commandline parameters.
			regVal = atoi(argv[2]);

			// Send new status register value to the flash programmer device.
			memStatus = SetupStatusRegisterValue(regVal);

			printf("\nUpdating status register value: %c", animSeq[0]);

			// Wait until memory erasing is complete.
			while((memStatus & 0x01) == 0x01)
			{
				sleep(200);
				memStatus = GetFlashMemoryStatus();

				// Update screen animation.
				printf("%c%c", 8, animSeq[animPos]);
				if((++animPos) > 3)
				{
					animPos = 0;
				}
			}

			// register value update operation completed.
			printf("%c",8);
			printf("completed.\n");

			return EXIT_SUCCESS;
		}
		else
		{
		  // Number of arguments are mismatching.
		  fprintf(stderr, ERR_PARAM);
		  return EXIT_FAILURE;
		}
	}
	else if((strcmp(argv[1], "-u") == 0) || (strcmp(argv[1], "--get-status") == 0))
	{
		// Display device status register values.
		unsigned char memStatus = 0;

		// Request device status register value from flash programmer device.
		memStatus = GetFlashMemoryStatus();

		printf("\nSRWD: %d\nBP0:  %d\nBP1:  %d\nBP2:  %d\n", ((memStatus & 0x80) >> 7), ((memStatus & 0x04) >> 2), ((memStatus & 0x08) >> 3), ((memStatus & 0x10) >> 4));
		return EXIT_SUCCESS;
	}
	else if((strcmp(argv[1], "-h") == 0) || (strcmp(argv[1], "--help") == 0))
	{
		// Show application help message.
		ShowApplicationHelp();
		return EXIT_SUCCESS;
	}
	else if((strcmp(argv[1], "-v") == 0) || (strcmp(argv[1], "--version") == 0))
	{
		// Show application version information.
		ShowApplicationVersion();
		return EXIT_SUCCESS;
	}
	else
	{
		// Unknown command.
		fprintf(stderr, ERR_PARAM);
		return EXIT_FAILURE;
	}

  return EXIT_SUCCESS;
}

/*****************************************************************************
 * Function: ShowApplicationHelp
 *****************************************************************************
 *
 * Function to show application help information.
 *
 * return: None.
 ****************************************************************************/
void ShowApplicationHelp()
{
  printf("\nUsage: flashprog [OPTION]...");
  printf("\n   or: flashprog [OPTION]\n");
  printf("\nFlashProg is programmer utility for low voltage SPI flash memory devices.\n");
  printf("\nValid options for flashprog are listed in below:\n");
  printf("\n -i, --information   List down flash memory device identifiers.\n");
  printf("\n -r, --read          Read specified address range from flash memory and ");
  printf("\n                     write to file.");
  printf("\n                      -r start-address length filename\n");
  printf("\n -e, --erase         Erase entire flash memory.\n");
  printf("\n -b, --block-erase   Erase block of flash memory.");
  printf("\n                      -b address\n");
  printf("\n -s, --sector-erase  Erase sector of flash memory.");
  printf("\n                      -s address\n");
  printf("\n -w, --write         Write binary file to flash memory.");
  printf("\n                      -w filename\n");
  printf("\n -g, --set-status    Set status register value.");
  printf("\n                      -g status-register-value\n");
  printf("\n -u, --get-status    Show flash memory status register information.\n");
  printf("\n -v, --version       Show version information.\n");
  printf("\n -h, --help          Display this help and exit.\n");
  printf("\nBefore perform any write operations execute full / block / sector erase to ");
  printf("\nset all flash memory bits to 1.\n");
  printf("\nlength parameter in read option is in bytes and it must be greater than 0.\n");
}

/*****************************************************************************
 * Function: ShowApplicationVersion
 *****************************************************************************
 *
 * Function to show application version and licensing information.
 *
 * return: None.
 ****************************************************************************/
void ShowApplicationVersion()
{
  printf("\nFlashProg Version 1.0.0\n");
  printf("\nFlashProg Copyright (C) 2015 Dilshan R Jayakody (4S6DRJ).");
  printf("\nLicense GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.");
  printf("\nThis is free software: you are free to change and redistribute it.");
  printf("\nThere is NO WARRANTY, to the extent permitted by law.\n");
  printf("\nWritten by Dilshan R Jayakody <dilshan@sourceforge.net>.\n");
}

/*****************************************************************************
 * Function: GetFlashMemoryStatus
 *****************************************************************************
 *
 * Return value of the Status Register of flash memory chip.
 *
 * return: Current Status Register value.
 ****************************************************************************/
unsigned char GetFlashMemoryStatus()
{
	usb_dev_handle* progHandle = NULL;
	unsigned char usbBuffer[USBPROG_BUFFER_SIZE], memStatus = 0;
	int byteCount = 0;

	// Open USB flash programmer device.
	progHandle = OpenUsbFlashProg();
	if(progHandle == NULL)
	{
		fprintf(stderr, ERR_USB_OPEN);
		goto exit;
	}

	// Send request to get the flash memory status data.
	byteCount = usb_control_msg(progHandle, (USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN), DEVREQ_GET_STATUS, 0, 0, (char*)usbBuffer, sizeof(usbBuffer), USBPROG_TIMEOUT);
	if(byteCount < 0)
	{
		printf("\nFlashProg: %s\n", usb_strerror());
		goto exit;
	}

	if(byteCount == 2)
	{
		// Check for compatible response type.
		if(usbBuffer[0] == DEVREQ_GET_STATUS)
		{
			memStatus = usbBuffer[1];
		}
		else
		{
			fprintf(stderr, ERR_USB_INVALID_RESPONSE);
			goto exit;
		}
	}
	else
	{
		fprintf(stderr, ERR_USB_STATUS_FAIL);
		goto exit;
	}

exit:
	// Close and cleanup all allocated resources.
	CloseUsbFlashProg(progHandle);
	return memStatus;
}

/*****************************************************************************
 * Function: SetupStatusRegisterValue
 *****************************************************************************
 *
 * Write specified value to Status Register of flash memory chip. Use this
 * function to update values of SRWD, BP0, BP1 and BP2 bits of Status
 * Register.
 *
 * @statusRegVal: New Status Register value.
 *
 * return: Current Status Register value with WIP bit.
 ****************************************************************************/
unsigned char SetupStatusRegisterValue(unsigned char statusRegVal)
{
	usb_dev_handle* progHandle = NULL;
	unsigned char usbBuffer[USBPROG_BUFFER_SIZE], memStatus = 0;
	int byteCount = 0;

	// Open USB flash programmer device.
	progHandle = OpenUsbFlashProg();
	if(progHandle == NULL)
	{
		fprintf(stderr, ERR_USB_OPEN);
		goto exit;
	}

	usbBuffer[0] = statusRegVal;

	// Send new status register value to the flash programmer device.
	byteCount = usb_control_msg(progHandle, (USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_OUT), DEVREQ_SET_STATUS, 0, 0, (char*)usbBuffer, 1, USBPROG_TIMEOUT);
	if(byteCount < 0)
	{
		printf("\nFlashProg: %s\n", usb_strerror());
		goto exit;
	}

	// Send Erase instruction to the flash programmer.
	byteCount = usb_control_msg(progHandle, (USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN), DEVREQ_UPDATE_STATUS, 0, 0, (char*)usbBuffer, sizeof(usbBuffer), USBPROG_TIMEOUT);
	if(byteCount < 0)
	{
		printf("\nFlashProg: %s\n", usb_strerror());
		goto exit;
	}

	if(byteCount == 2)
	{
		// Check for compatible response type.
		if(usbBuffer[0] == DEVREQ_UPDATE_STATUS)
		{
			memStatus = usbBuffer[1];
		}
		else
		{
			fprintf(stderr, ERR_USB_INVALID_RESPONSE);
			goto exit;
		}
	}
	else
	{
		fprintf(stderr, ERR_USB_STATUS_FAIL);
		goto exit;
	}

exit:
	// Close and cleanup all allocated resources.
	CloseUsbFlashProg(progHandle);
	return memStatus;
}

/*****************************************************************************
 * Function: EraseFlashMemorySegment
 *****************************************************************************
 *
 * Erase block or sector of flash memory which is related to preset address.
 *
 * @eraseCommand: Erase mode (DEVREQ_ERASE_BLOCK or DEVREQ_ERASE_SECTOR).
 *
 * return: Current Status Register value with WIP bit.
 ****************************************************************************/
unsigned char EraseFlashMemorySegment(unsigned char eraseCommand)
{
	usb_dev_handle* progHandle = NULL;
	unsigned char usbBuffer[USBPROG_BUFFER_SIZE], memStatus = 0;
	int byteCount = 0;

	// Open USB flash programmer device.
	progHandle = OpenUsbFlashProg();
	if(progHandle == NULL)
	{
		fprintf(stderr, ERR_USB_OPEN);
		goto exit;
	}

	// Send Erase instruction to the flash programmer.
	byteCount = usb_control_msg(progHandle, (USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN), eraseCommand, 0, 0, (char*)usbBuffer, sizeof(usbBuffer), USBPROG_TIMEOUT);
	if(byteCount < 0)
	{
		printf("\nFlashProg: %s\n", usb_strerror());
		goto exit;
	}

	if(byteCount == 2)
	{
		// Check for compatible response type.
		if(usbBuffer[0] == eraseCommand)
		{
			memStatus = usbBuffer[1];
		}
		else
		{
			fprintf(stderr, ERR_USB_INVALID_RESPONSE);
			goto exit;
		}
	}
	else
	{
		fprintf(stderr, ERR_USB_STATUS_FAIL);
		goto exit;
	}

exit:
	// Close and cleanup all allocated resources.
	CloseUsbFlashProg(progHandle);
	return memStatus;
}

/*****************************************************************************
 * Function: EraseFlashMemory
 *****************************************************************************
 *
 * This function issue request to erase entire flash memory chip.
 *
 * return: Current Status Register value with WIP bit.
 ****************************************************************************/
unsigned char EraseFlashMemory()
{
	usb_dev_handle* progHandle = NULL;
	unsigned char usbBuffer[USBPROG_BUFFER_SIZE], memStatus = 0;
	int byteCount = 0;

	// Open USB flash programmer device.
	progHandle = OpenUsbFlashProg();
	if(progHandle == NULL)
	{
		fprintf(stderr, ERR_USB_OPEN);
		goto exit;
	}

	// Send Erase instruction to the flash programmer.
	byteCount = usb_control_msg(progHandle, (USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN), DEVREQ_ERASE, 0, 0, (char*)usbBuffer, sizeof(usbBuffer), USBPROG_TIMEOUT);
	if(byteCount < 0)
	{
		printf("\nFlashProg: %s\n", usb_strerror());
		goto exit;
	}

	if(byteCount == 2)
	{
		// Check for compatible response type.
		if(usbBuffer[0] == DEVREQ_ERASE)
		{
			memStatus = usbBuffer[1];
		}
		else
		{
			fprintf(stderr, ERR_USB_INVALID_RESPONSE);
			goto exit;
		}
	}
	else
	{
		fprintf(stderr, ERR_USB_STATUS_FAIL);
		goto exit;
	}

exit:
	// Close and cleanup all allocated resources.
	CloseUsbFlashProg(progHandle);
	return memStatus;
}

/*****************************************************************************
 * Function: SetFlashWriteAddress
 *****************************************************************************
 *
 * Preset global flash memory address on flash programmer device. This preset
 * address is used in read, write and block or sector erases requests.
 *
 * @startAddr: New 24 bit start address.
 *
 * return: None
 ****************************************************************************/
void SetFlashWriteAddress(unsigned int startAddr)
{
	usb_dev_handle* progHandle = NULL;
	int byteCount = 0;
	unsigned char usbBuffer[USBPROG_BUFFER_SIZE];

	// Open USB flash programmer device.
	progHandle = OpenUsbFlashProg();
	if(progHandle == NULL)
	{
		fprintf(stderr, ERR_USB_OPEN);
		goto exit;
	}

	// Send address range to flash programmer.
	usbBuffer[0] = startAddr & 0xFF;
	usbBuffer[1] = (startAddr >> 8) & 0xFF;
	usbBuffer[2] = (startAddr >> 16) & 0xFF;
	usbBuffer[3] = 0;

	byteCount = usb_control_msg(progHandle, (USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_OUT), DEVREQ_SET_PARAM, 0, 0, (char*)usbBuffer, 4, USBPROG_TIMEOUT);
	if(byteCount < 0)
	{
		printf("\nFlashProg: %s\n", usb_strerror());
		goto exit;
	}
	
exit:
	// Close and cleanup all allocated resources.
	CloseUsbFlashProg(progHandle);
}

/*****************************************************************************
 * Function: WriteFlashMemoryPage
 *****************************************************************************
 *
 * Send 256 bytes of data buffer to flash programmer device to write into
 * page buffer of the flash memory chip.
 *
 * @dataBuffer: Pointer to data buffer.
 * @dataBufferSize: Size of the data buffer in bytes.
 *
 * return: Current Status Register value with WIP bit.
 ****************************************************************************/
unsigned char WriteFlashMemoryPage(unsigned char* dataBuffer, int dataBufferSize)
{
	usb_dev_handle* progHandle = NULL;
	unsigned char usbBuffer[USBPROG_BUFFER_SIZE], memStatus = 0;
	int byteCount = 0;

	// Open USB flash programmer device.
	progHandle = OpenUsbFlashProg();
	if(progHandle == NULL)
	{
		fprintf(stderr, ERR_USB_OPEN);
		goto exit;
	}

	// Send data buffer to flash programmer.
	byteCount = usb_control_msg(progHandle, (USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_OUT), DEVREQ_SET_BUFFER, 0, 0, (char*)dataBuffer, dataBufferSize, USBPROG_TIMEOUT);
	if(byteCount < 0)
	{
		printf("\nFlashProg: %s\n", usb_strerror());
		goto exit;
	}

	sleep(5);
	// Sending page write request to flash programmer.
	byteCount = usb_control_msg(progHandle, (USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN), DEVREQ_WRITE, 0, 0, (char*)usbBuffer, sizeof(usbBuffer), USBPROG_TIMEOUT);
	if(byteCount < 0)
	{
		printf("\nFlashProg: %s\n", usb_strerror());
		goto exit;
	}

	if(byteCount == 2)
	{
		// Check for compatible response type.
		if(usbBuffer[0] == DEVREQ_WRITE)
		{
			memStatus = usbBuffer[1];
		}
		else
		{
			fprintf(stderr, ERR_USB_INVALID_RESPONSE);
			goto exit;
		}
	}
	else
	{
		fprintf(stderr, ERR_USB_STATUS_FAIL);
		goto exit;
	}

exit:
	// Close and cleanup all allocated resources.
	CloseUsbFlashProg(progHandle);
	return memStatus;
}

/*****************************************************************************
 * Function: ReadFlashMemoryBlock
 *****************************************************************************
 *
 * This function send request to read flash memory values from specified
 * address range and return received data buffer.
 *
 * @startAddr: Starting address to perform read operation.
 * @addrRange: Number of bytes to read from flash memory chip.
 * @dataBuffer: Pointer to data buffer.
 *
 * return: Number of bytes in output buffer.
 ****************************************************************************/
unsigned char ReadFlashMemoryBlock(unsigned int startAddr, unsigned char addrRange, unsigned char** dataBuffer)
{
	usb_dev_handle* progHandle = NULL;
	int byteCount = 0;
	unsigned char usbBuffer[USBPROG_BUFFER_SIZE], readByteCount = 0;

	// Open USB flash programmer device.
	progHandle = OpenUsbFlashProg();
	if(progHandle == NULL)
	{
		fprintf(stderr, ERR_USB_OPEN);
		goto exit;
	}

	// Send address range to flash programmer.
	usbBuffer[0] = startAddr & 0xFF;
	usbBuffer[1] = (startAddr >> 8) & 0xFF;
	usbBuffer[2] = (startAddr >> 16) & 0xFF;
	usbBuffer[3] = addrRange;

	byteCount = usb_control_msg(progHandle, (USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_OUT), DEVREQ_SET_PARAM, 0, 0, (char*)usbBuffer, 4, USBPROG_TIMEOUT);
	if(byteCount < 0)
	{
		printf("\nFlashProg: %s\n", usb_strerror());
		goto exit;
	}

	// Send read request to the programmer and obtain data buffer.
	byteCount = usb_control_msg(progHandle, (USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN), DEVREQ_READ_BLOCK, 0, 0, (char*)usbBuffer, sizeof(usbBuffer), USBPROG_TIMEOUT);
	if(byteCount < 0)
	{
		printf("\nFlashProg: %s\n", usb_strerror());
		goto exit;
	}

	(*dataBuffer) = (unsigned char*)malloc(byteCount);
	if((*dataBuffer) == NULL)
	{
		fprintf(stderr, ERR_MEM_FAIL);
		goto exit;
	}

	memcpy((*dataBuffer), usbBuffer, byteCount);
	readByteCount = byteCount;

exit:
	// Close and cleanup all allocated resources.
	CloseUsbFlashProg(progHandle);
	return readByteCount;
}

/*****************************************************************************
 * Function: GetFlashMemoryDeviceID
 *****************************************************************************
 *
 * Get flash memory manufacture ID and device ID from memory chip and print
 * on standard output stream.
 *
 * return: None
 ****************************************************************************/
void GetFlashMemoryDeviceID()
{
	usb_dev_handle* progHandle = NULL;
	int byteCount = 0;
	unsigned char usbBuffer[USBPROG_BUFFER_SIZE];

	// Open USB flash programmer device.
	progHandle = OpenUsbFlashProg();
	if(progHandle == NULL)
	{
		fprintf(stderr, ERR_USB_OPEN);
		goto exit;
	}

	// Send flash ID read command to the programmer device.
	byteCount = usb_control_msg(progHandle, (USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN), DEVREQ_FLASHMEM_ID, 0, 0, (char*)usbBuffer, sizeof(usbBuffer), USBPROG_TIMEOUT);
	if(byteCount < 0)
	{
		printf("\nFlashProg: %s\n", usb_strerror());
		goto exit;
	}

	if(byteCount == 4)
	{
		// Check for compatible response type.
		if(usbBuffer[0] == DEVREQ_FLASHMEM_ID)
		{
			printf("\nManufacture ID: 0x%x\nMemory Type:    0x%x\nMemory density: 0x%x\n", usbBuffer[1], usbBuffer[2], usbBuffer[3]);
		}
		else
		{
			fprintf(stderr, ERR_USB_INVALID_RESPONSE);
			goto exit;
		}
	}
	else
	{
		fprintf(stderr, ERR_USB_FLASH_ID_FAIL);
		goto exit;
	}

exit:
	// Close and cleanup all allocated resources.
	CloseUsbFlashProg(progHandle);
}

/*****************************************************************************
 * Function: OpenUsbFlashProg
 *****************************************************************************
 *
 * Open USB flash programmer device and return device handle for further
 * I/O operations.
 *
 * return: USB device handle, or NULL if device is not available.
 ****************************************************************************/
usb_dev_handle* OpenUsbFlashProg()
{
	struct usb_bus* bus;
	struct usb_device* dev;

	usb_init();
	usb_find_busses();
	usb_find_devices();

	for (bus = usb_get_busses(); bus; bus = bus->next)
	{
		for (dev = bus->devices; dev; dev = dev->next)
		{
			if((dev->descriptor.idVendor == USBPROG_VID) && (dev->descriptor.idProduct == USBPROG_PID))
			{
				return usb_open(dev);
			}
		}
	}

	return NULL;
}

/*****************************************************************************
 * Function: CloseUsbFlashProg
 *****************************************************************************
 *
 * This function try to close specified USB device handle.
 *
 * @devHandle: USB device handle.
 *
 * return: None.
 ****************************************************************************/
void CloseUsbFlashProg(usb_dev_handle* devHandle)
{
	if(devHandle != NULL)
	{
		usb_close(devHandle);
	}
}
