/*************************************************************************
 * FlashProg ATmega8A firmware.
 *
 * Copyright (C) 2015 Dilshan R Jayakody (4S6DRJ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/

#ifndef FLASHPROG_H_
#define FLASHPROG_H_

// Set CPU clock to 12.000MHz.
#define F_CPU 12000000UL

// USB control request codes.
#define DEVCMD_FLASHMEM_ID		0x01
#define DEVCMD_SET_PARAM		0x02
#define DEVCMD_READ_BLOCK		0x03
#define DEVCMD_ERASE			0x04
#define DEVCMD_GET_STATUS		0x05
#define DEVCMD_SET_BUFFER		0x06
#define DEVCMD_WRITE			0x07
#define DEVCMD_ERASE_BLOCK		0x08
#define DEVCMD_ERASE_SECTOR		0x09
#define DEVCMD_SET_STATUS		0x0A
#define DEVCMD_UPDATE_STATUS	0x0B

// Flash memory command set.
#define FLASHMEM_RDID	0x9F
#define FLASHMEM_READ	0x03
#define FLASHMEM_WREN	0x06
#define FLASHMEM_CE		0xC7
#define FLASHMEM_RDSR	0x05
#define FLASHMEM_PP		0x02
#define FLASHMEM_BE		0xD8
#define FLASHMEM_SE		0x20
#define FLASHMEM_WRSR	0x01

// Size of flash memory output page buffer.
#define PAGEBUFFER_SIZE 256

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

unsigned char readLength, currentInst, bufferPos, statusReg;
unsigned char pageBuffer[PAGEBUFFER_SIZE];
unsigned short bufferSpace;
unsigned long int startAddress;

void InitSystem();
void EraseFlashMem();
void ClearDataBuffer();
void WriteFlashMemPage();
void InitSPIMasterMode();
void UpdateStatusRegister();
void ExecSPIWrite(unsigned char commandByte);
void EraseFlashMemSeg(unsigned char eraseCode);

unsigned char GetFlashMemID(unsigned char* dataBuffer, unsigned char commandID);
unsigned char ReadFlashMemBlock(unsigned char* dataBuffer);
unsigned char GetFlashMemStatus();

#endif /* FLASHPROG_H_ */