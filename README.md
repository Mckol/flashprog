# FlashProg

## Introduction

FlashProg is a USB flash memory programmer designed to work with 25x series 3.3V SPI flash memory devices, which are commonly used to store BIOS in PC motherboards.

FlashProg is built around the Atmel ATmega8A (**not** ATmega8 or ATmega8L) microcontroller. The loader supports 32 bit and 64 bit Windows and Linux operating systems.

All the source code and hardware design files are available in this repository under the [GNU General Public License version 3.0](http://www.gnu.org/licenses/gpl-3.0.en.html). 

[The project wiki](https://gitlab.com/LunarEclipse363/flashprog/-/wikis/home) contains most of the documentation for this project.

## Improvements over upstream

- Rewritten the documentation.
- Changed the directory structure to be more intuitive.
- Added a makefile for the firmware for easy compilation on Linux.
- **(TODO)** Added instructions for using this programmer with 1.8V 25x series chips.
- **(TODO)** Added instructions on using the JSPI1 header on MSI motherboards for ISP.

# Credits 

- [Dilshan R Jayakody](https://sourceforge.net/u/dilshan/profile/): Creating the [original project on SourceForge](https://sourceforge.net/projects/flashprog/).
- [LunarEclipse](https://gitlab.com/LunarEclipse363): Changes in this repository. 
